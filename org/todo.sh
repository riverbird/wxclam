#!/bin/bash
org_file=${1}
bak_file="${1}~"
cry_file="${org_file}.des"
#echo ${cry_file}
openssl enc -des -d -a -in ${cry_file} -out ${org_file}
emacsclient -t ${org_file}
openssl enc -des -e -a -in ${org_file} -out ${cry_file}
rm -f ${org_file}
rm -f ${bak_file}
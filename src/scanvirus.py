#!/usr/bin/python
#coding:utf8

import pyclamav
import os,pdb
class cls_scan:
	def __init__(self):
		self.lst_path = []
	def scan_file(self,filename):
		#pdb.set_trace()
		dct_result = dict()
		dct_result['is_virus'] = 'OK'
		dct_result['detail'] = ''
		try:
			ret = pyclamav.scanfile(filename)
		except ValueError,e:
			dct_result['detail'] = str(e)
			return dct_result
		except TypeError,e:
			dct_result['detail'] = str(e)
			return dct_result
		if cmp(ret[0],0) == 0:
			dct_result['is_virus'] = 'OK'
			dct_result['detail'] = 'OK'
		else:
			dct_result['is_virus'] = 'VIRUS'
			dct_result['detail'] = ret[1]
		return dct_result
	def list_fold(self,path):
		try:
			for i in os.listdir(path):
				str_path = os.path.join(path,i)
				if not os.path.isdir(str_path):
					self.lst_path.append(str_path)
				else:
					self.list_fold(str_path)
		except Exception,e:
			pass
	def scan_fold(self,fold):
		pdb.set_trace()

if __name__ == '__main__':
	obj_scan = cls_scan()
	obj_scan.list_fold('/home/riverbird/temp/a')
	lst_result = obj_scan.lst_path
	print lst_result

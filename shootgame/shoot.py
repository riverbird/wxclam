#!/usr/bin/python
#coding:utf-8
"""
@Author: Zhang Hong
@Created on: 6th Dec
"""
#============================
# import modules
import pygame
from pygame.locals import *

if not pygame.font: print 'Warning:fonts disabled'
if not pygame.mixer: print 'Warning: sound disabled'
import sys,os
import time
try:
    import psyco
    psyco.full()
except:
    print "没有启动psyco加速。"
    
import core

#===========================
SCREENRECT     = Rect(0, 0, 800, 600)

global score
score = 0
#sprites = pygame.sprite.RenderPlain()

#===========================
# function defination
def load_image(name,colorkey = None):
	try:
        	image = pygame.image.load(name)
        except pygame.error,message:
        	print "Can't load image:",name
                raise SystemExit,message
        image = image.convert()
        if colorkey is not None:
        	if colorkey is -1: 
                	colorkey = image.get_at((0,0))
                image.set_colorkey(colorkey,RLEACCEL)
        return image,image.get_rect()
def load_font(txt):
        font = pygame.font.Font(u"font/simsun.ttc",20)
        text = font.render(txt,1,(255,0,0))
        textpos = text.get_rect()
        return text,textpos
def load_sound(name):
        try:
        	sound = pygame.mixer.Sound(name)
        except pygame.error,message:
        	print "Can't load sound:", name
                raise SystemExit,message
        return sound
def main_1():
        obj_frame = core.gameframe()
    	bk1,bkrec1 = obj_frame.load_image("img/bk1.png")
    	obj_frame.screen.blit(bk1,(0,0))

    	bk = pygame.Surface(obj_frame.screen.get_size())
    	bk = bk.convert()
    	bk.fill((250,250,250))
    	obj_frame.screen.blit(bk,(0,0))
    	pygame.display.flip()
    	obj_frame.run()

def main():
	#pygame init
	pygame.init()
        screen = pygame.display.set_mode((800,600))
        pygame.display.set_caption("Yulang Shoot Game")
        pygame.mouse.set_visible(1)
	#create background
	background = pygame.Surface(screen.get_size())
	background = background.convert()
	background.fill((250,250,250))
	#define variables
	clock = pygame.time.Clock()
    	btn = core.button()
	sprites = pygame.sprite.RenderPlain((btn))

	#Introduction Picture
        bk_intro,bk_intro_rec = load_image("img/bk1.png")
    	screen.blit(bk_intro,(0,0))
        pygame.display.flip()

        #Group
        grp_btn = pygame.sprite.Group()
        all = pygame.sprite.RenderUpdates()

        running = True
        #pygame.display.update()

        while running:
		dt = clock.tick(60)
        	#sprites.update(dt/1000.0)
        	#sprites.update()
                for event in pygame.event.get():
               		if event.type == pygame.QUIT:
                        	running = False
                        elif (event.type == pygame.KEYDOWN) and (event.type == pygame.K_ESCAPE):
                        	running = False
                        elif event.type == pygame.MOUSEBUTTONDOWN:
                        	running = False
                        elif event.type is pygame.MOUSEBUTTONUP:
                        	running = False

                screen.blit(bk_intro, (0, 0))
                screen.blit(btn,(10,10))
		#all.clear(screen, bk_intro)
                #all.update()
		#screen.fill((255,255,255))
		#sprites.draw(screen)
		#pygame.display.flip()
	pygame.quit()

if __name__ == '__main__':
    main()

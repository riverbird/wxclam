#!/usr/bin/python
#coding: utf-8
import cocos
from cocos.actions import *
from cocos.director import director

import pyglet
from pyglet.gl import *

from cocos.director import *
from cocos.menu import *
from cocos.scene import *
from cocos.layer import *
from cocos.actions import *
from cocos.sprite import Sprite
'''
try:
	import psyco
	psyco.full()
except:
	print '没有启动psyco加速.'
'''

class HelloWorld(cocos.layer.Layer):
	def __init__(self):
		super(HelloWorld,self).__init__()
		label = cocos.text.Label("Hello,world",
				font_name = "Times New Roman",
				font_size = 32,
				anchor_x = "center",anchor_y="center")
		label.position = 320,240		
		self.add(label)
		sprite = cocos.sprite.Sprite('img/btn_start_1.png')
		sprite.position = 320,240
		sprite.scale = 3
		self.add(sprite,z=1)
		scale = ScaleBy(3,duration=2)

class MouseDisplay(cocos.layer.Layer):
	is_event_handler = True
	def __init__(self):
		super(MouseDisplay,self).__init__()
		self.posx = 100
		self.posy = 240
		self.text = cocos.text.Label("No mouse events yet",font_size = 18,x=self.posx,y=self.posy)
		self.add(self.text)
	def update_text(self,x,y):
		text = "Mouse @ %d,%d" % (x,y)
		self.text.element.text = text
		self.text.element.x = self.posx
		self.text.element.y = self.posy
	def on_mouse_motion(self,x,y,dx,dy):
		self.update_text(x,y)
	def on_mouse_press(self,x,y,buttons,modifiers):
		self.posx,self.posy = director.get_virtual_coordinates(x,y)
		self.update_text(x,y)

class MainMenu(Menu):
    def __init__( self ):
        # call superclass with the title
        super( MainMenu, self ).__init__("Yulang Shoot Game" )
        pyglet.font.add_directory('.')

        # you can override the font that will be used for the title and the items
        self.font_title['font_name'] = 'You Are Loved'
        self.font_title['font_size'] = 72

        self.font_item['font_name'] = 'You Are Loved'
        self.font_item_selected['font_name'] = 'You Are Loved'

        # you can also override the font size and the colors. see menu.py for
        # more info

        # example: menus can be vertical aligned and horizontal aligned
        self.menu_valign = CENTER
        self.menu_halign = CENTER

        items = []

        items.append( MenuItem('New Game', self.on_new_game ) )
        items.append( MenuItem('Options', self.on_options ) )
        items.append( MenuItem('Scores', self.on_scores ) )
        items.append( MenuItem('Quit', self.on_quit ) )

        self.create_menu( items, zoom_in(), zoom_out() )

    # Callbacks
    def on_new_game( self ):
	    # director.set_scene( StartGame() )
	    print "on_new_game()"
           

    def on_scores( self ):
        self.parent.switch_to( 2 )

    def on_options( self ):
        self.parent.switch_to( 1 )

    def on_quit( self ):
        director.pop()
	
if __name__ == "__main__":
	#cocos.director.director.init()
	#hello_layer = HelloWorld()
	#main_scene = cocos.scene.Scene(hello_layer)
	#cocos.director.director.run(main_scene)

	director.set_depth_test()
	menulayer = MultiplexLayer( MainMenu() )
	scene =Scene( menulayer )
	
	director.init(resizable=True)
	director.run(cocos.scene.Scene(MouseDisplay()))

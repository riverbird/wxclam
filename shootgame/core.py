#coding:utf-8
import os,sys
import pygame
from pygame.locals import *

class gameframe():
	def __init__(self):
		pygame.init()
		self.screen = pygame.display.set_mode((800,600))
		pygame.display.set_caption("Yulang Shoot Game")
		pygame.mouse.set_visible(1)
	def run(self):
		running = True
		pygame.display.update()
		while running:
			sprites.update()
			#event = pygame.event.wait()
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					running = False
				elif (event.type == pygame.KEYDOWN) and (event.type == pygame.K_ESCAPE):
					running = False
				elif event.type == pygame.MOUSEBUTTONDOWN:
					sys.exit()
				elif event.type is pygame.MOUSEBUTTONUP:
					pass
			sprites.draw(screen)
		pygame.quit()
	def load_image(self,name,colorkey = None):
		try:
			image = pygame.image.load(name)
		except pygame.error,message:
			print "Can't load image:",name
			raise SystemExit,message
		image = image.convert()
		if colorkey is not None:
			if colorkey is -1:
				colorkey = image.get_at((0,0))
			image.set_colorkey(colorkey,RLEACCEL)
		return image,image.get_rect()
	def load_font(txt):
		font = pygame.font.Font(u"font/simsun.ttc",20)
		text = font.render(txt,1,(255,0,0))
		textpos = text.get_rect()
		return text,textpos
	def load_sound(name):
		try:
			sound = pygame.mixer.Sound(name)
		except pygame.error,message:
			print "Can't load sound:", name
			raise SystemExit,message
		return sound

class button(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load("img/button.png").convert()
		self.rect = self.image.get_rect()
	def update(self):
		for event in pygame.event.get([MOUSEBUTTONDOWN,MOUSEBUTTONUP]):
			if event.type == MOUSEBUTTONDOWN:
				pass

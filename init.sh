#!/bin/bash
emacs_ver=23.1

mv ~/.emacs ~/.emacs_bk
ln -s ~/wxclam/.emacs ~/.emacs
#===check os===

#--Mandriva--
os_line=`sed -n '1p' /etc/issue`
os_name=`expr substr "$os_line" 1 8`
if [ $os_name = "Mandriva" ]; then
	echo "OS name: ${os_name}"
	sudo urpmi aumix
fi

#--Arch--
os_line=`sed -n '2p' /etc/issue`
ln_len=`echo "${os_line}" | wc -m`
#echo $ln_len
#if [ -n ${os_line} ]; then
if [ $ln_len != 1 ]; then
	os_name=`expr substr "$os_line" 1 4`
	if [ $os_name = "Arch" ]; then
		echo "OS name: ${os_name}"
		sudo pacman -S aumix
	fi
fi
sudo cp -r lisp/emacsim/* /usr/share/emacs/${emacs_ver}/leim


#--opensuse--
os_line=`sed -n '1p' /etc/issue`
echo $os_line
startpos=$(echo $os_line|awk '{print index($os_line,"openSUSE")}')
echo $startpos
if [ $startpos != 0 ]; then
	os_name=`expr substr "$os_line" $startpos 8`
	echo "OS name: ${os_name}"
	sudo zypper in aumix
fi

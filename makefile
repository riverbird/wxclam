update-svn:
	svn up
update-suse:
	sudo zypper ref
	sudo zypper up
update-mandriva:
	sudo urpmi --auto-update
update-debian:
	sudo aptitude update
	sudo aptitude upgrade
